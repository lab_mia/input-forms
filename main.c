#include <stdio.h>
#include <unistd.h>

int main (int argc, char* argv[]) {
   char str[60];

   for(int i = 0; i<60; i++){
     str[i]='\0';
   }
   printf("------------ Input por argumentos ----------------- ");
   printf("No. de argumentos: %d; Argumentos:\n", argc);
   for(int i=0; i<argc; i++){
     printf("%d). %s;\n", i, argv[i]);
   }

   
   printf("------------ Input por stdin (standard input) -------- ");
   /* opening file for reading */
   while( read(0, str, 10)) {
     printf("%s",str);
     for(int i = 0; i<60; i++){
       str[i]='\0';
     }
   }
   printf("\n");
   
   return(0);
}
